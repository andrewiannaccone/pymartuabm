import numpy
import datetime

import sim.geography
import util
import sim.spinifex
import sim.grass
import sim.fire
import sim.rain

from PyQt4.QtGui import *

black = numpy.uint32(0x00000000)
blue1 = numpy.uint32(0x00000001)
red = numpy.uint32(0x00ff0000)
orange = numpy.uint32(0x00ff9933)
white = numpy.uint32(0x00ffffff)
green = numpy.uint32(0x0000ff00)
blue = numpy.uint32(0x000000ff)


class FireAgeView:
    def __init__(self, g: sim.geography.Geography, firesim: sim.fire.FireSim):
        self.geo = g
        self.firesim = firesim
        self.max_age = datetime.timedelta(days=10*365)

        self.weights = numpy.zeros(shape=(g.width, g.height), dtype=float)
        self.rgb = numpy.zeros(shape=(g.width, g.height),
                               dtype=numpy.uint32)
        for (x, y) in self.geo.firebreaks:
            self.rgb[y, x] = white

    def qimage(self):
        s = self.firesim.state

        extinction_to_weight = {}

        for (x, y) in s.extinctions:
            xt = s.extinctions[x, y]
            if xt not in extinction_to_weight:
                time_since_fire = self.firesim.time - s.extinctions[x, y]
                w = 1 - min(time_since_fire / self.max_age, 1)
                extinction_to_weight[xt] = w
            else:
                w = extinction_to_weight[xt]

            if w != self.weights[x][y]:
                self.weights[x][y] = w
                self.rgb[y, x] = util.rgb_weighted(orange, black, w)

        for (x, y) in s.ignitions:
            self.rgb[y, x] = red

        return QImage(self.rgb, self.geo.width, self.geo.height,
                      QImage.Format_RGB32)


class RainView:
    def __init__(self, g: sim.geography.Geography, rainsim: sim.rain.RainSim):
        self.geo = g
        self.rainsim = rainsim

        self.weights = numpy.zeros(shape=(g.width, g.height), dtype=float)
        self.rgb = numpy.zeros(shape=(g.width, g.height),
                               dtype=numpy.uint32)
        self.w_to_rgb = {}

        for (x, y) in self.geo.firebreaks:
            self.rgb[y, x] = white

    def qimage(self):
        rs = self.rainsim.state
        fs = self.rainsim.firesim.state

        for c in self.geo.all_cells():
            t_last_fire = fs.extinctions[c]
            rain_since = rs.rain_since(t_last_fire)

            w = min(rain_since / 1000.0, 1.0)

            (x, y) = c
            if w != self.weights[x][y]:
                self.weights[x][y] = w

                if w in self.w_to_rgb:
                    wcolor = self.w_to_rgb[w]
                else:
                    wcolor = util.rgb_weighted(blue, black, w)
                    self.w_to_rgb[w] = wcolor

                self.rgb[y, x] = wcolor

        for (x, y) in fs.ignitions:
            self.rgb[y, x] = red

        return QImage(self.rgb, self.geo.width, self.geo.height,
                      QImage.Format_RGB32)


class GrassView:
    def __init__(self, g: sim.geography.Geography, grasssim: sim.grass.GrassSim):
        self.geo = g
        self.grasssim = grasssim

        self.weights = numpy.zeros(shape=(g.width, g.height), dtype=float)
        self.rgb = numpy.zeros(shape=(g.width, g.height),
                               dtype=numpy.uint32)
        self.w_to_rgb = {}

        for (x, y) in self.geo.firebreaks:
            self.rgb[y, x] = white

    def qimage(self):
        s = self.grasssim.firesim.state

        for c in self.geo.all_cells():
            w = self.grasssim.cover_percent(c)

            # 256 distinct shades of green is plenty
            w = int(w*256) / 256

            (x, y) = c
            if w != self.weights[x][y]:
                self.weights[x][y] = w

                if w in self.w_to_rgb:
                    wcolor = self.w_to_rgb[w]
                else:
                    wcolor = util.rgb_weighted(green, black, w)
                    self.w_to_rgb[w] = wcolor

                self.rgb[y, x] = wcolor

        for (x, y) in s.ignitions:
            self.rgb[y, x] = red

        return QImage(self.rgb, self.geo.width, self.geo.height,
                      QImage.Format_RGB32)


class SpinifexView:
    def __init__(self, g: sim.geography.Geography, spinifexsim: sim.spinifex.SpinifexSim):
        self.geo = g
        self.spinifexsim = spinifexsim

        self.weights = numpy.zeros(shape=(g.width, g.height), dtype=float)
        self.rgb = numpy.zeros(shape=(g.width, g.height),
                               dtype=numpy.uint32)
        self.w_to_rgb = {}

        for (x, y) in self.geo.firebreaks:
            self.rgb[y, x] = white

    def qimage(self):
        s = self.spinifexsim.firesim.state

        for c in self.geo.all_cells():
            w = self.spinifexsim.cover_percent(c)

            # 256 distinct shades of green is plenty
            w = int(w*256) / 256

            (x, y) = c
            if w != self.weights[x][y]:
                self.weights[x][y] = w

                if w in self.w_to_rgb:
                    wcolor = self.w_to_rgb[w]
                else:
                    wcolor = util.rgb_weighted(green, black, w)
                    self.w_to_rgb[w] = wcolor

                self.rgb[y, x] = wcolor

        for (x, y) in s.ignitions:
            self.rgb[y, x] = red

        return QImage(self.rgb, self.geo.width, self.geo.height,
                      QImage.Format_RGB32)
