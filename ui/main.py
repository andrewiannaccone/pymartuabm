import csv
import time
from threading import RLock
import datetime
from ui import mapview

from PyQt4.QtCore import *
from PyQt4.QtGui import *
import numpy

import sim.master
import sim.wind
from ui.widgets import ScaledSlider, Compass


click_to_start_fire = False

year = datetime.timedelta(days=365)
black = 0x00000000
green1 = 0x00000100
blue1 = 0x00000001
red = 0x00ff0000
white = 0x00ffffff


class UIModel:
    def __init__(self, m: sim.master.MartuABMSimulation, gui):
        self.lock = RLock()
        self.m = m
        self.gui = gui

        self.frame_interval = None

        self.land_arr = numpy.ndarray(shape=(self.m.geography.width,
                                             self.m.geography.height),
                                      dtype=numpy.uint32)
        for x in range(self.m.geography.width):
            for y in range(self.m.geography.height):
                self.land_arr[x, y] = black
        self.land_img = QImage(self.land_arr.data, self.m.geography.width,
                               self.m.geography.height, QImage.Format_RGB32)
        self.wind_vector = (0, 0)
        self.wind_str = '--'
        self.time_str = '--'
        self.dots = []

        self.faview = mapview.FireAgeView(self.m.geography, self.m.firesim)
        self.sview = mapview.SpinifexView(self.m.geography, self.m.spinifexsim)
        self.gview = mapview.GrassView(self.m.geography, self.m.grasssim)
        self.rview = mapview.RainView(self.m.geography, self.m.rainsim)
        self.active_view = self.faview

    def update_from_model(self):
        t = self.m.time
        with self.lock:
            self.land_img = self.active_view.qimage()

            self.wind_vector = self.m.windsim.state.as_vector()
            self.wind_str = str(self.m.windsim.state)
            self.time_str = str(t)
            self.gui.update()

    def frame_dt(self):
        return datetime.timedelta(days=self.frame_interval)


class MartuGUI(QFrame):

    def __init__(self, sim: sim.master.MartuABMSimulation):
        super().__init__()
        self.sim = sim
        self.w = sim.geography.width
        self.h = sim.geography.height
        self.uidata = UIModel(self.sim, self)
        self.mw = SimThread(self.sim, self.uidata, self)
        self.fps = 1

        self.paused = True

        grid = QGridLayout()

        self.landscape_label = Landscape(self)
        self.landscape_label.setScaledContents(True)
        self.landscape_label.setFixedSize(1000, 1000)
        grid.addWidget(self.landscape_label, 0, 0, 1, 5)

        self.compass_frame = Compass(self)
        grid.addWidget(self.compass_frame, 1, 0, 4, 2)

        self.wind_label = QLabel(self)
        grid.addWidget(self.wind_label, 6, 0)

        self.time_label = QLabel(self)
        grid.addWidget(self.time_label, 7, 0)

        self.display_button_frame = QFrame()
        rblayout = QHBoxLayout()
        self.fire_view_button = QRadioButton('Fire')
        rblayout.addWidget(self.fire_view_button)
        self.rain_view_button = QRadioButton('Rain')
        rblayout.addWidget(self.rain_view_button)
        self.grass_view_button = QRadioButton('Grass')
        rblayout.addWidget(self.grass_view_button)
        self.spinifex_view_button = QRadioButton('Spinifex')
        rblayout.addWidget(self.spinifex_view_button)
        self.display_button_frame.setLayout(rblayout)
        grid.addWidget(self.display_button_frame, 1, 2, 1, 4)

        grid.addWidget(QLabel('Frame Interval:', self), 3, 1)
        self.frame_interval_slider = ScaledSlider(Qt.Horizontal, self)
        grid.addWidget(self.frame_interval_slider, 3, 2, 1, 2)
        self.frame_interval_display = QLabel(self)
        grid.addWidget(self.frame_interval_display, 3, 4)

        grid.addWidget(QLabel('Strike Rate:', self), 4, 1)
        self.strike_slider = ScaledSlider(Qt.Horizontal, self)
        grid.addWidget(self.strike_slider, 4, 2, 1, 2)
        self.strike_display = QLabel(self)
        grid.addWidget(self.strike_display, 4, 4)

        grid.addWidget(QLabel('Spread Factor:', self), 5, 1)
        self.spread_slider = ScaledSlider(Qt.Horizontal, self)
        grid.addWidget(self.spread_slider, 5, 2, 1, 2)
        self.spread_display = QLabel(self)
        grid.addWidget(self.spread_display, 5, 4)

        self.play_button = QPushButton('Play', self)
        grid.addWidget(self.play_button, 6, 1)
        self.pause_button = QPushButton('Pause', self)
        grid.addWidget(self.pause_button, 6, 2)
        self.write_age_button = QPushButton('Write Age Matrix', self)
        grid.addWidget(self.write_age_button, 6, 3)

        self.setLayout(grid)
        self.setWindowTitle("Martu ABM")
        self.resize(1000, 1300)
        self.move(0, 0)
        self.show()

        # Bind widgets to model values
        self.bind_data()

    def set_frame_interval(self, val):
        self.uidata.frame_interval = val
        self.uidata.next_frame_time = self.sim.time + self.uidata.frame_dt()
        # TODO: pause/reset repainting timer?
        if val > 365:
            vdisp = "%3.1f yrs" % (val / 365.25)
        elif val > 45:
            vdisp = "%3.1f months" % (val / 30)
        elif val > 1:
            vdisp = "%3.1f days" % val
        elif val > 0.5:
            vdisp = "%02d hours" % (24 * val)
        else:
            vdisp = "%3.1f hours" % (24 * val)
        self.frame_interval_display.setText(vdisp)

    def set_strike_rate(self, val):
        factor = datetime.timedelta(days=1) / self.sim.firesim.interval
        self.strike_display.setText(str(round(factor * 365 * val, 1)) + '/yr')
        self.sim.firesim.strike_rate = val

    def set_spread_f(self, val):
        self.spread_display.setText(str(round(val, 2)))
        self.sim.firesim.f = val

    def start_fire(self, x, y):
        self.sim.firesim.start_fire(x, y)

    def write_age_matrix(self):
        t = self.sim.time
        t_months = 12 * t.year + t.month
        with open('age.csv', 'w') as fd:
            w = csv.writer(fd)
            for y in range(self.sim.geography.height):
                row = list()
                for x in range(self.sim.geography.width):
                    ex_months = self.sim.firesim.state.extinction_months[(x, y)]
                    age_months = t_months - ex_months
                    row.append(age_months)
                w.writerow(row)

    def bind_data(self):

        self.connect(self.frame_interval_slider, SIGNAL('scaledValueChanged(double)'),
                     self.set_frame_interval)
        self.frame_interval_slider.setRange(0.05, 6*30)
        self.frame_interval_slider.setValue(30)

        self.connect(self.strike_slider, SIGNAL('scaledValueChanged(double)'),
                     self.set_strike_rate)
        self.strike_slider.setRange(0.00, 0.005)
        self.strike_slider.setValue(self.sim.firesim.strike_rate)

        self.connect(self.spread_slider, SIGNAL('scaledValueChanged(double)'),
                     self.set_spread_f)
        self.spread_slider.setRange(0.05, 5.00)
        self.spread_slider.setValue(self.sim.firesim.f)

        self.connect(self.play_button, SIGNAL('clicked()'), self.play)
        self.play_button.setEnabled(self.paused)

        self.connect(self.pause_button, SIGNAL('clicked()'), self.pause)
        self.pause_button.setEnabled(not self.paused)

        self.connect(self.write_age_button, SIGNAL('clicked()'),
                     self.write_age_matrix)

        self.connect(self.fire_view_button, SIGNAL('toggled(bool)'),
                     self.view_button_toggle)
        self.connect(self.grass_view_button, SIGNAL('toggled(bool)'),
                     self.view_button_toggle)
        self.connect(self.rain_view_button, SIGNAL('toggled(bool)'),
                     self.view_button_toggle)
        self.connect(self.spinifex_view_button, SIGNAL('toggled(bool)'),
                     self.view_button_toggle)

    def view_button_toggle(self, checked):
        if checked:
            if self.spinifex_view_button.isChecked():
                print('VEG')
                self.uidata.active_view = self.uidata.sview
            elif self.grass_view_button.isChecked():
                print('GRASS')
                self.uidata.active_view = self.uidata.gview
            elif self.rain_view_button.isChecked():
                print('RAIN')
                self.uidata.active_view = self.uidata.rview
            elif self.fire_view_button.isChecked():
                print('FIRE')
                self.uidata.active_view = self.uidata.faview
            else:
                raise Exception

            print("Update GUI %s" % self.sim.time.strftime('%m/%d/%y %H:%M'))
            self.mark_ready()

    def play(self):
        self.play_button.setEnabled(False)
        self.pause_button.setEnabled(True)
        # self.update_timer.start(1000 / self.)
        self.mw.restart()

    def pause(self):
        self.play_button.setEnabled(True)
        self.pause_button.setEnabled(False)
        # self.update_timer.stop()
        self.mw.pause()

    def mark_ready(self):
        self.uidata.update_from_model()

    def paintEvent(self, ev: QPaintEvent):
        qpm = QPixmap(self.uidata.land_img)
        self.landscape_label.setPixmap(qpm)
        self.wind_label.setText(self.uidata.wind_str)
        self.time_label.setText(self.uidata.time_str)
        super().paintEvent(ev)


class Landscape(QLabel):
    def __init__(self, *__args):
        super().__init__(*__args)

    def mousePressEvent(self, qme: QMouseEvent):
        if click_to_start_fire:
            p = qme.pos()
            x, y = p.x(), p.y()
            # TODO: fix this ugly kluge
            x = int(x/2)
            y = int(y/2)
            print('Starting a fire at (%s, %s)' % (x, y))
            self.parent().start_fire(x, y)


class SimThread(QThread):
    def __init__(self, sim: sim.master.MartuABMSimulation, uidata: UIModel, gui: MartuGUI):
        super().__init__()
        self.sim = sim
        self.uidata = uidata
        self.gui = gui
        self.paused = False

        self.next_frame_time = None

    def pause(self):
        self.paused = True

    def restart(self):
        if self.paused:
            self.paused = False
        else:
            self.uidata.next_frame_time = self.sim.time + self.uidata.frame_dt()
            self.start()

    def run(self):
        while True:
            if self.paused:
                time.sleep(1)
            else:
                with self.uidata.lock:
                    ntt = self.sim.next_transition_time()
                    # print("%s --> %s " %
                    #       (self.sim.time.strftime('%m/%d/%y %H:%M'),
                    #        ntt.strftime('%m/%d/%y %H:%M')))
                    self.sim.advance_to(ntt)
                    if self.sim.time >= self.uidata.next_frame_time:
                        # TODO rethink threading... can we keep the sim running in another core?
                        print("Update GUI %s" % self.sim.time.strftime('%m/%d/%y %H:%M'))
                        self.uidata.next_frame_time = self.sim.time + self.uidata.frame_dt()
                        self.gui.mark_ready()

