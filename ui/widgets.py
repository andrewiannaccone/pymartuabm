from PyQt4.QtCore import SIGNAL, SLOT, pyqtSignal, pyqtSlot, Qt
from PyQt4.QtGui import QSlider, QFrame, QPaintEvent, QPainter, QPen, QBrush,\
                        QHBoxLayout, QLabel


def proportion(x, xmin, xmax):
    return (float(x) - xmin) / xmax


class ScaledSlider(QSlider):

    def __init__(self, *args):
        super().__init__(*args)

        # max/min as used internally by QSlider
        super().setMinimum(0)
        super().setMaximum(1000000)

        # The scaled min/max values.  Adjustable with setRange().
        self._vmin = 0
        self._vmax = 100
        self._vrange = self._vmax - self._vmin

        # Translate and reemit valueChanged signals as scaledValueChange signals
        self.connect(self, SIGNAL('valueChanged(int)'),
                     SLOT('reemit_value_changed(int)'))

    scaledValueChanged = pyqtSignal(float)

    def setMinimum(self, value):
        self.setRange(value, self._vmax)

    def setMaximum(self, value):
        self.setRange(self._vmin, value)

    def setRange(self, vmin, vmax):
        old_value = self.value()
        self._vmin = vmin
        self._vmax = vmax
        self._vrange = vmax - vmin
        self.setValue(old_value)

    def value(self):
        p = proportion(super().value(), self.minimum(), self.maximum())
        return self._vmin + p * self._vrange
        # return float(super().value()) / self._max_int * self._vrange

    def setValue(self, value):
        p = proportion(value, self._vmin, self._vmax)
        v = self.minimum() + p * (self.maximum() - self.minimum())
        super().setValue(int(v))

    @pyqtSlot(int)
    def reemit_value_changed(self):
        self.scaledValueChanged.emit(self.value())


class SliderPanel(QFrame):
    def __init__(self, label, minval, maxval, *args):
        super().__init__(*args)

        self.slider = ScaledSlider(Qt.Horizontal, self)
        self.slider.setMinimum(minval)
        self.slider.setMaximum(maxval)
        self.display = QLabel(self)

        layout = QHBoxLayout()
        layout.addWidget(QLabel(label, self))
        layout.addWidget(self.slider)
        layout.addWidget(self.display)
        self.setLayout(layout)


class Compass(QFrame):
    def __init__(self, parent):
        super().__init__(parent)
        self.uidata = parent.uidata
        self.setFixedSize(200, 200)

    def paintEvent(self, ev: QPaintEvent):
        p = QPainter(self)
        rect = self.contentsRect()
        bevel = 3
        w = min(rect.height(), rect.width()) - 2 * bevel
        r = w / 2
        center_x = bevel + r
        center_y = bevel + r

        p.setPen(QPen(QBrush(Qt.darkGray, Qt.Dense4Pattern), bevel))
        p.setBrush(QBrush(Qt.white))
        p.drawEllipse(bevel, bevel, w, w)
        p.drawEllipse(center_x - 3, center_y - 3, 6, 6)

        p.setPen(QPen(Qt.black, 3))
        (dx, dy) = self.uidata.wind_vector
        p.drawLine(center_x, center_y, center_x + r * dx, center_y - r * dy)
