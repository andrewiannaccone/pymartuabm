import math
import bunch3
import datetime


class SpinifexSim:
    """
    There's no transition here.  We calculate cover percent on demand.
    """

    def __init__(self, rainsim, firesim, **params):
        self.rainsim = rainsim
        self.firesim = firesim
        p = bunch3.bunchify(params)

    # From Southgate and Carfew, 2007 (Table 3):
    #     cover_pct = exp(u) / (1 + exp(u))
    #     u = -7.119
    #        + 30.932 * 10^-3 * (mean annual rainfall in mm)
    #        - 0.413 * 10^4 * (mean annual rain in mm)^2
    #        + 0.055 * (years since last fire)
    #
    def cover_percent(self, cell):
        t_last_fire = self.firesim.state.extinctions[cell]
        days_since = self.rainsim.time.toordinal() - t_last_fire.toordinal()
        y = days_since / 365.25
        # y = (self.rainsim.time - t_last_fire) / datetime.timedelta(days=365.25)

        # Mean annual rainfall
        # TODO parameterize this
        # TODO verify that the second regression term is actually this squared
        mar = 350

        u = -7.119
        u += 30.932 * 10**(-3) * mar
        u += -0.413 * 10**(-4) * mar**2
        u += 0.055 * y

        cp = math.exp(u) / (1 + math.exp(u))
        return cp
