import sim.rain
import datetime
import sim.fire
import math


class GrassSim:
    """
    There's no transition here.  We calculate cover percent on demand.
    """

    def __init__(self, rainsim: sim.rain.RainSim, firesim: sim.fire.FireSim, **params):
        self.rainsim = rainsim
        self.firesim = firesim

    # From Southgate and Carfew, 2007 (Table 4, ignoring substrate):
    #     cover_pct = exp(u) / (1 + exp(u))
    #     u = -1.574
    #        - 0.099 * (years since last fire)
    #        + 0.0039 * (rainfall prev. 12 months <in mm?>)
    #        + 0.620 * (Substrate (d+s) <1 for drainage or sand plain, elso 0?>)
    #
    def cover_percent(self, cell):
        t_last_fire = self.firesim.state.extinctions[cell]
        days_since = self.rainsim.time.toordinal() - t_last_fire.toordinal()
        y = days_since / 365.25

        t_year_ago = self.rainsim.time - datetime.timedelta(days=365)
        t_cutoff = max(t_last_fire, t_year_ago)
        past_year_rain = self.rainsim.state.rain_since(t_cutoff)

        # TODO: implement
        substrate = 0

        u = -1.574
        u += -0.099 * y
        u += 0.0039 * past_year_rain
        u += 0.620 * substrate

        cp = math.exp(u) / (1 + math.exp(u))
        return cp
