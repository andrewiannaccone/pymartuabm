import itertools
import abc
import random

import math


class Geography:
    def __init__(self, cell_width=15, width=100, height=100, firebreaks=set()):
        self.cell_width = cell_width            # Cell width (in meters)
        self.width = width                      # Map width (number of cells)
        self.height = height                    # Map height (number of cells)
        self.firebreaks = firebreaks

    def cell_count(self):
        return self.width * self.height

    def random_cell(self):
        x = random.randrange(self.width)
        y = random.randrange(self.height)
        return x, y

    def all_cells(self):
        return [(x, y) for x, y in
                itertools.product(range(self.width), range(self.height))]

    def neighbors(self, cell):
        out = set()
        x, y = cell
        for (dx, dy) in itertools.product([-1, 0, 1], [-1, 0, 1]):
            i = x + dx
            j = y + dy
            if (i, j) != (x, y)\
                    and 0 <= i < self.width and 0 <= j < self.height:
                out.add((i, j))
        return out

    @abc.abstractclassmethod
    def get_patch(self, cell):
        """
        :param cell: tuple
        :return: the Patch that owns this cell
        """
        return NotImplemented


class SuperPixelGridGeography(Geography):

    def __init__(self, cell_width=None, width=None, height=None, sp_width=None, sp_height=None,
                 firebreaks=set()):
        """
        :param cell_width:  in meters
        :param width:       number of cells along X-axis of entire geography
        :param height:      number of cells along Y-axis of entire geography
        :param sp_width:    number of cells along X-axes of each rectangular superpixel block
        :param sp_width:    number of cells along Y-axes of each rectangular superpixel block
        :param firebreaks:
        """
        super().__init__(cell_width, width, height, firebreaks)
        self.sp_width = sp_width
        self.sp_height = sp_height

        Nx = int(math.ceil(self.width/self.sp_width))
        Ny = int(math.ceil(self.height/self.sp_height))
        self.patches = list()
        for x in range(Nx):
            px = list()
            for y in range(Ny):
                xrange = range(Nx * sp_width, (Nx+1) * sp_width)
                yrange = range(Nx * sp_height, (Nx+1) * sp_height)
                cells = [(i, j) for i in xrange for j in yrange]
                px.append(Patch(cells))
            self.patches.append(px)

    def get_patch(self, cell):
        x, y = cell
        i = int(x/self.sp_width)
        j = int(y/self.sp_height)
        return self.patches[i][j]


class Patch:
    def __init__(self, cells):
        self.cells = cells
