import bisect
import sim

import bunch3
import util

import csv
import datetime


class RainState:
    def __init__(self, queue_length):
        import collections

        self.rains = collections.deque([], queue_length)
        self.times = collections.deque([], queue_length)
        self._rain_since = {}

        self.tmap = {}

    def add_rain_event(self, t, height):
        if len(self.times) == self.times.maxlen:
            t0 = self.times[0]
            del self._rain_since[t0]
        self.rains.append(height)
        self.times.append(t)

        self._rain_since[t] = height
        for tx in self.times:
            self._rain_since[tx] += height

    def rain_since(self, t):
        # TODO -- AI: I think there's a bug in here
        i = self.tmap.get(t)
        if not i:
            i = bisect.bisect_left(self.times, t)
            if i < len(self.times):
                self.tmap[t] = i
        if i == len(self.times):
            return 0
        else:
            return self._rain_since[self.times[i]]


class RainSim(sim.DiscreteTimeSimulator):
    def __init__(self, start_time, firesim, **params):
        super().__init__(start_time)
        self.firesim = firesim

        p = bunch3.bunchify(params)

        self.dailyRain = parse_australia_weather_file(p.file)

        self.height = p.height
        self.prob = p.prob

        self.interval = util.timedelta_from_str(p.interval)
        dt_hist = util.timedelta_from_str(p.history)
        q_len = int(dt_hist / self.interval)
        self.state = RainState(q_len)

        self.last_trans_time = start_time

    def transition(self):
        t = self.next_transition_time()
        # 1% chance of exactly 30 millimeters of rain
        # rain_height = self.height if random.random() < self.prob else 0

        # Rather than make a random draw as previously, look up daily rainfall
        # in the dictionary of daily rain
        # rain_height = self.dailyRain[t]
        rain_height = self.dailyRain.get(t, 0)

        self.state.add_rain_event(t, rain_height)
        self.last_trans_time = t
        self.time = t

    def next_transition_time(self):
        return self.last_trans_time + self.interval


def parse_australia_weather_file(file_name):
    # Parse an input file containing climate data in the format used by the Australian Bureau of Meteorology. Right now
    # only rainfall data is returned (as a dictionary with datetime objects as the key).
    #
    # First four lines of file [first line is wrapped]:
    # Product code,Bureau of Meteorology station number,Year,Month,Day,Rainfall amount (millimetres),
    # [...]Period over which rainfall was measured (days),Quality
    # IDCJAC0009,013030,1974,01,01,0.0,,Y
    # IDCJAC0009,013030,1974,01,02,0.0,,Y
    # IDCJAC0009,013030,1974,01,03,0.0,,Y
    #
    # A very small number of measurements are the rainfall measured over two adjacent days. All such rain is put
    # in a single day, rather than spread over the days (i.e., not correction is made). This leaves the total rainfall
    # unaffected.
    daily_rain = dict()
    with open("data/" + file_name, "r") as csvFile:
        rain_reader = csv.reader(csvFile, delimiter=",", quotechar="\"")
        next(rain_reader, None)  # skip the header line
        for row in rain_reader:
            key = datetime.datetime(int(row[2]), int(row[3]), int(row[4]))

            # The input file can contain empty data for daily rain, for which precipitation is set to 0
            precip = row[5]
            if precip == "":
                daily_rain[key] = 0
            else:
                daily_rain[key] = float(precip)

    # # There appear to be skipped dates in the input weather file. Therefore, iterate from the
    # # first date to the last date in daily_rain and add missing dates (with precipation = 0).
    # firstDate = min(daily_rain.keys())
    # lastDate = max(daily_rain.keys())
    # dt = lastDate - firstDate
    # numDays = dt.days
    # for i in range(0,numDays):
    #     d = firstDate + datetime.timedelta(i)
    #     #if not d in daily_rain:
    #     #    daily_rain[d] = 0
    return daily_rain
