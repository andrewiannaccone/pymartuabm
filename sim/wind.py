import csv
import math
import numpy
import datetime
import sim


class WindState:
    class SpeedCat:
        MAX_MAG = 13.0

        def __init__(self, label, mag):
            self.label = label
            self.mag = mag

    class Direction:
        def __init__(self, label, grid_vect, angle_pirads):
            self.label = label
            self.angle_pirads = angle_pirads
            self.nbr_vect = grid_vect

        def radians(self):
            return math.pi * self.angle_pirads

    def __init__(self, index, speedcat: SpeedCat, direction: Direction):
        self.index = index
        self.speedcat = speedcat
        self.direction = direction

    def as_vector(self, max_length=1):
        length = max_length * self.speedcat.mag / WindState.SpeedCat.MAX_MAG
        dx = math.cos(self.direction.radians()) * length
        dy = math.sin(self.direction.radians()) * length
        return dx, dy

    def __str__(self):
        return str(self.speedcat.mag) + ' ' + self.direction.label


WindState.SPEEDCATS = {
    'CALM': WindState.SpeedCat('CALM', 0.0),
    '0_to_2': WindState.SpeedCat('0_to_2', 1.0),
    '2_to_5': WindState.SpeedCat('2_to_5', 3.5),
    '5_to_8': WindState.SpeedCat('5_to_8', 6.5),
    '8_to_11': WindState.SpeedCat('8_to_11', 9.5),
    '11_and_over': WindState.SpeedCat('11_and_over', 13)
}

# Using coordinate where +x is right, +y is down
WindState.DIRECTIONS = {
    'CALM': WindState.Direction('CALM', None, 0.0),
    'N': WindState.Direction('N', (0, -1), 0.50),
    'NE': WindState.Direction('NE', (1, -1), 0.25),
    'E': WindState.Direction('E', (1, 0), 0),
    'SE': WindState.Direction('SE', (1, 1), -0.25),
    'S': WindState.Direction('S', (0, 1), -0.50),
    'SW': WindState.Direction('SW', (-1, 1), -0.75),
    'W': WindState.Direction('W', (-1, 0), -1),
    'NW': WindState.Direction('NW', (-1, -1), 0.75)
}

WindState.STATES = {}
with open('config/wind/columns.csv') as col_fd:
    reader = csv.reader(col_fd, delimiter=',', quotechar='"')
    next(reader)
    for i_str, s_lab, d_lab in reader:
        WindState.STATES[int(i_str)] = WindState(int(i_str),
                                                 WindState.SPEEDCATS[s_lab],
                                                 WindState.DIRECTIONS[d_lab])


class WindSim(sim.DiscreteTimeSimulator):
    def __init__(self, start_time: datetime.datetime):
        super().__init__(start_time)
        self.time = start_time

        self.trans_per_day = 2
        self.hours_per_trans = math.floor(24/self.trans_per_day)
        assert self.trans_per_day * self.hours_per_trans == 24

        # Markov transition matrices, by time of day and season
        self.tmats = {}
        self.steady_states = {}
        for s in ['ew', 'lw', 'ed', 'ld']:
            for h in ['T9', 'T15']:
                with open('config/wind/' + h + s + '.csv') as fd:
                    m = numpy.zeros((41, 41))
                    for row_num, col_str in zip(range(41), fd.readlines()):
                        m[row_num] = col_str.split(',')
                    for i in range(41):
                        assert abs(sum(m[i, :]) - 1) < 0.0001
                    self.tmats[(s, h)] = numpy.transpose(m)

            # Markov matrix for the day-on-day transition
            t09 = self.tmats[(s, 'T9')]
            t15 = self.tmats[(s, 'T15')]
            t_day = t09.dot(t15)

            # Seasonal steady states arising from the day-on-day transition
            v, d = numpy.linalg.eig(numpy.transpose(t_day))
            i = list(v).index(max(v))
            ss = numpy.real(d[:, i])
            ss = ss / sum(ss)
            # TODO: verify that max(v)=1 and sum(ss) = 1
            self.steady_states[s] = ss

        # Random initial wind state, drawn from seasonal steady-state dist'n
        ss = self.steady_states[self.season()]
        state_index = numpy.random.choice(len(ss), p=ss)
        self.state = WindState.STATES[state_index]

    def season(self):
        m = self.time.month
        if m in (11, 12, 1):
            return 'ew'
        elif m in (2, 3, 4):
            return 'lw'
        elif m in (5, 6, 7):
            return 'ed'
        else:
            return 'ld'

    def next_transition_time(self):
        t = self.time.replace(minute=0, second=0, microsecond=0)

        i_trans = math.floor(t.hour/self.hours_per_trans) + 1
        hour_ntt = i_trans * self.hours_per_trans

        if hour_ntt == 24:
            t_ntt = t + datetime.timedelta(days=1)
            t_ntt = t_ntt.replace(hour=0)
        else:
            t_ntt = t.replace(hour=hour_ntt)

        return t_ntt

    # Advance simulator to next transition time
    def transition(self):
        # print('Wind transition')
        # Advance to noon or midnight
        self.time = self.next_transition_time()

        # Transition using T09 during AM, then T15 during PM
        if self.time.hour > 12:
            tmat = self.tmats[(self.season(), 'T15')]
        else:
            tmat = self.tmats[(self.season(), 'T9')]

        # TODO: revert or confirm this change
        # Transition using T09 at noon, then T15 at midnight
        # if self.time.hour == 12:
        #     tmat = self.tmats[(self.season(), 'T9')]
        # elif self.time.hour == 0:
        #     tmat = self.tmats[(self.season(), 'T15')]
        # else:
        #     raise ValueError('Impossible transition time: ' + self.time.hour)

        # Markov transition to a new wind state
        # v = self.state.unit_vector()
        # state_index = numpy.random.choice(len(v), p=tmat.dot(v))
        state_index = numpy.random.choice(41, p=tmat[:, self.state.index])
        self.state = WindState.STATES[state_index]
        # TODO: remove
        # self.state.direction = WindState.DIRECTIONS['W']
        # print(self.state)
