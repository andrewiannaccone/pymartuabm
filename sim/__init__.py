import abc
import datetime


class DiscreteTimeSimulator:
    """
    A simulator based on discrete transitions at specific times.  Responsible
    for knowing when its next transition occurs.
    """
    def __init__(self, start_time: datetime.datetime):
        self.time = start_time

    @abc.abstractmethod
    def transition(self, **kwargs):
        raise NotImplementedError

    @abc.abstractmethod
    def next_transition_time(self):
        raise NotImplementedError

    def advance_to(self, t: datetime.datetime):
        while self.next_transition_time() <= t:
            self.transition()
        self.time = t
