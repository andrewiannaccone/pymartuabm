import datetime
import sim
import sim.geography
import sim.wind
import sim.fire
import sim.rain
import sim.spinifex
import sim.grass


def ntt_fn(dtm):
    return dtm.next_transition_time()


class MartuABMSimulation:
    def __init__(self, start_time, config):
        """
        Top-level simulator for the Martu Agent Based Model (I'm using the word
        "simulator" instead of "model" in comments, since "model" usually refers
        to the data backing a UI view).

        Instantiate all of the sub-simulators (Fire, Wind, etc) using the
        DiscreteTimeSimulator class.

        :param start_time: Arbitrary start date
        :param config: Simulator parameters (as bunch object, not dict)
        :return:
        """
        self.time = start_time
        p = config
        self.geography = sim.geography.SuperPixelGridGeography(**p.landscape)
        self.subsims = []

        self.windsim = sim.wind.WindSim(self.time)
        self.firesim = sim.fire.FireSim(self.time, self.geography,
                                        self.windsim, **p.fire)
        self.rainsim = sim.rain.RainSim(self.time, self.firesim, **p.rain)
        self.spinifexsim = sim.spinifex.SpinifexSim(self.rainsim, self.firesim)
        self.grasssim = sim.grass.GrassSim(self.rainsim, self.firesim)

        self.subsims.append(self.windsim)
        self.subsims.append(self.firesim)
        self.subsims.append(self.rainsim)

    def next_transition_time(self):
        return min(map(ntt_fn, self.subsims))

    def advance_by(self, dt: datetime.timedelta):
        self.advance_to(self.time + dt)

    def advance_to(self, t_end: datetime.datetime):
        """
        Advance all sub-simulators, in order, until they reach time t_end.

        :param t_end:
        :return:
        """

        # Find the sub-simulator that will be the next to transition (i.e. the
        # next to actually do something e.g. wind changing at 12 PM).  Advance
        # it to that transition time.  If the sub-simulator is a
        # DiscreteTimeSimulator, this will be accomplished by asking it to
        # perform one transition.  Repeat until no sub-simulators can transition
        # again without advancing past t_end.

        # TODO: deal rigorously with multiple sub-sims acting simultaneously

        ntt = self.next_transition_time()
        while ntt <= t_end:
            # Find the sub-simulator with the earliest next transition.
            s = min(filter(None, self.subsims), key=ntt_fn)

            # Advance that simulator by one transition
            s.advance_to(ntt)

            # Find the next transition time after th
            ntt = min(map(ntt_fn, self.subsims))

        # Finally, ask each sub-simulator to "advance" to t_end.  For sub-
        # simulators of class DiscreteTimeSimulator, this should involve no
        # futher transitions.  It just means setting the clock forward on each
        # sub-simulator, so that it agrees with this object about the current
        # time, elapsed time since an event, etc.
        for m in self.subsims:
            m.advance_to(t_end)
        self.time = t_end
