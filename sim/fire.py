import datetime
import math
import random
import numpy
from numpy.linalg import norm
import bunch3
import sim
import sim.geography
import sim.wind
import util


year = datetime.timedelta(days=365)


class FireState:
    def __init__(self):
        self.ignitions = {}
        self.extinctions = {}

        # Redundant and imprecise, but useful for "time since last fire"
        # calculations.  Those calculations have to be performed on each of the
        # NxM cells at each UI update (to determine the vegetation color),
        # so they have to be fast.
        self.extinction_months = {}


class FireSim(sim.DiscreteTimeSimulator):
    def __init__(self, start_time, geography: sim.geography.Geography,
                 wind: sim.wind.WindSim, **params):
        super().__init__(start_time)
        self.last_transition_time = None
        # Months in which lightning can occur (could be an input param):
        # NOV,DEC,JAN,FEB,MAR,APR
        self.lightning_months = (1, 2, 3, 4, 11, 12)
        self.geography = geography
        self.wind = wind

        p = bunch3.bunchify(params)

        # Interval between fire spread trials
        self.interval = util.timedelta_from_str(p.dt)

        # How long a fire burns
        self.duration = util.timedelta_from_str(p.duration)

        # Fire spread model parameters (Haydon et al., 2000)
        self.k_b = p.haydon.k_b
        self.f = p.haydon.F
        self.w = p.haydon.W
        self.s = p.haydon.S

        # Poisson lightning ignitions per interval (rate over entire geography)
        self.strike_rate = (p.strike_rate_km_day
                            * (geography.cell_width / 1000) ** 2
                            * geography.cell_count()
                            * (self.interval / datetime.timedelta(days=1)))

        # Bounds for (randomly initialized) times since last fire
        min_init_age = util.timedelta_from_str(p.min_init_age)
        max_init_age = util.timedelta_from_str(p.max_init_age)

        # Initialize cell states
        self.state = FireState()
        for cell in geography.all_cells():
            r = random.random()
            a = min_init_age + r * (max_init_age - min_init_age)
            self.state.extinctions[cell] = start_time - a
            self.state.extinction_months[cell] = (
                12 * self.state.extinctions[cell].year +
                self.state.extinctions[cell].month)

        self.to_ignite = []

        self.cached_cos = {}
        self.cached_bwb = {}
        self.cached_ap_age = {}

    def start_fire(self, x, y):
        self.to_ignite.append((x, y))

    # As modeled in Haydon et al 2000, Appendix 2
    def caching_cos(self, u, v):
        u, v = tuple(u), tuple(v)
        if (u, v) in self.cached_cos:
            cos = self.cached_cos[(u, v)]
        else:
            npu = numpy.array(u)
            npv = numpy.array(v)
            cos = round(npu.dot(npv) / (norm(npu) * norm(npv)), 2)
            self.cached_cos[(u, v)] = cos
        return cos

    def caching_bwb_calc(self, w_ws, u, v):
        if (w_ws, u, v) in self.cached_bwb:
            return self.cached_bwb[(w_ws, u, v)]
        q = math.exp(-self.k_b * w_ws)
        b4 = 0.125 * q
        b3 = (0.125 + (0.125 * 0.5 * (1 - q))) * q
        b2 = (0.125 + (0.125 + 0.125 * 0.5 * (1 - q)) * (1 - q)) * q
        b1 = (0.125 + (0.125 + (0.125 + 0.125 * 0.5 * (1 - q)) * (1 - q)) * (
            1 - q)) * q
        b0 = 0.125 + 2 * (
            0.125 + (0.125 + (0.125 + 0.125 * 0.5 * (1 - q)) * (1 - q)) * (
                1 - q)) * (1 - q)
        b0_check = 1 - 2 * b1 - 2 * b2 - 2 * b3 - b4
        # TODO remove this verification
        assert (abs(b0 - b0_check) < 0.00001)
        # END TODO
        if self.wind.state.direction.nbr_vect is None:
            b_wb = 0
        else:
            cos = self.caching_cos(u, v)
            b_wb = {
                1.00: b0,  # same direction
                0.71: b1,  # 45 degrees
                0.00: b2,  # 90 degrees
                -0.71: b3,  # 135 degrees
                -1.00: b4  # opposite directions
            }[cos]
        self.cached_bwb[(w_ws, u, v)] = b_wb
        return b_wb

    def caching_ap_age(self, age_months):
        if age_months in self.cached_ap_age:
            return self.cached_ap_age[age_months]
        else:
            age = age_months / 12.0
            a_age = 1 - math.exp(-(0.76 * age) ** 0.23) if age > 3 else 0
            n = self.duration / self.interval
            ap_age = 1 - math.exp(math.log(1 - a_age) / n)
            self.cached_ap_age[age_months] = ap_age
            return ap_age

    def ap_age(self, neighbor_cell):
        age = (self.time - self.state.extinctions[neighbor_cell]) / year
        a_age = 1 - math.exp(-(0.76 * age) ** 0.23) if age > 3 else 0
        n = self.duration / self.interval
        ap_age = 1 - math.exp(math.log(1 - a_age) / n)
        return ap_age

    def spread_prob(self, fire_cell, neighbor_cell):
        if neighbor_cell in self.state.ignitions:
            return 0

        # Map wind strength to spread probability factor S_wind_strength
        s_ws = self.s[self.wind.state.speedcat.label]

        # Map wind-to-spread-direction angle to spread prob factor B_wind_bias
        w_ws = self.w[self.wind.state.speedcat.label]
        u = (neighbor_cell[0] - fire_cell[0],
             neighbor_cell[1] - fire_cell[1])
        v = self.wind.state.direction.nbr_vect
        b_wb = self.caching_bwb_calc(w_ws, u, v)

        # Map age of neighbor cell vegetation to spread prob factor A'_age
        # NOTE: using approximate age (to month) for faster computation
        # ap_age = self.caching_ap_age(neighbor_cell)
        t_months = 12 * self.time.year + self.time.month
        age_months = t_months - self.state.extinction_months[neighbor_cell]
        ap_age = self.caching_ap_age(age_months)

        sp = s_ws * ap_age * b_wb * self.f
        return sp

    # Advance simulator state to next transition time
    def transition(self):
        self.time = self.next_transition_time()

        to_ignite = set()
        to_extinguish = set()

        # fires spread to neighboring cells
        for cell in self.state.ignitions.keys():
            for nbr in self.geography.neighbors(cell):
                p = self.spread_prob(cell, nbr)
                if random.random() < p:
                    to_ignite.add(nbr)

        # old fires burn out
        for cell, ignition_time in self.state.ignitions.items():
            if self.time >= ignition_time + self.duration:
                to_extinguish.add(cell)

        # lightning ignites new fires
        # noinspection PyTypeChecker

        # Only light fires during the specified months
        if self.time.month in self.lightning_months:
            for i in range(numpy.random.poisson(self.strike_rate)):
                c = self.geography.random_cell()
                to_ignite.add(c)
                if c[0] > 0 and c[1] > 0:
                    to_ignite.add((c[0], c[1] - 1))
                    to_ignite.add((c[0] - 1, c[1]))
                    to_ignite.add((c[0] - 1, c[1] - 1))

        # The following ignitions are from elsewhere (not lightning)
        # and so should not be excluded in non-lightning season months.
        for c in self.to_ignite:
            to_ignite.add((c[0], c[1]))
            if c[0] > 0 and c[1] > 0:
                to_ignite.add((c[0], c[1] - 1))
                to_ignite.add((c[0] - 1, c[1]))
                to_ignite.add((c[0] - 1, c[1] - 1))
        self.to_ignite.clear()

        for cell in to_ignite:
            if cell not in self.geography.firebreaks:
                self.state.ignitions[cell] = self.time

        for cell in to_extinguish:
            del self.state.ignitions[cell]
            self.state.extinctions[cell] = self.time
            self.state.extinction_months[cell] = (
                12 * self.state.extinctions[cell].year +
                self.state.extinctions[cell].month)

        self.last_transition_time = self.time

    def next_transition_time(self):
        last_time = self.last_transition_time or self.time
        return last_time + self.interval
