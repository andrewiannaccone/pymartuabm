import datetime
from PyQt4.QtGui import QApplication
import sys
import yaml
import bunch3
from sim import master
from ui.main import MartuGUI


def main(use_profiler=False):
    if use_profiler:
        print('Using profiler')
        sys.path.append('/Applications/PyVmMonitor.app/Contents/MacOS/public_api')
        # noinspection PyUnresolvedReferences
        import pyvmmonitor
        pyvmmonitor.connect()
    else:
        print('Not using profiler')

    # Load model configuration paramaters
    with open('config/models.yml') as fd:
        config = bunch3.bunchify(yaml.load(fd))
    m = master.MartuABMSimulation(datetime.datetime(2000, 1, 1), config)

    # Start the GUI
    app = QApplication(sys.argv)
    gui = MartuGUI(m)
    gui.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    print(sys.argv)
    useprof = ('pyvm' in sys.argv)
    main(use_profiler=useprof)
