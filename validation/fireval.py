import os
import re
import gdal
import numpy as np
from scipy.ndimage import measurements
from scipy.ndimage import find_objects
from scipy.ndimage.morphology import generate_binary_structure
from skimage.measure import perimeter
from sim import master
import datetime
import calendar
import subprocess
import math
import copy

# The firescar data effectively consists of two types of information, each of
# which can be represented as binary arrays or rasters: (1) the fire scars and
# (2) masks indicating whether a given pixel is in the lightning,
# anthropogenic, or mixed regime. Data is by date, in which each date records
# new firescars which appeared since the previous date for which data exists.
#
# The raw data resides in three directories, but one of the directories is
# redundant in the sense that it can be reconstructed from the other two.
# Hence, the required input to the Firescars class is simply the following two
# folders:
#
# scarDir        A directory with all firescars by date
# regimeDir        A directory with firescars in the lightning and anthropogenic
#                 regimes by date. The masks are deduced from this directory.
#
# In addition, an optional intermediate products directory, prodDir, can be
# passed to Firescars at initialization. Storing the intermediate products
# substantially speeds up access to the data. Although using prodDir is
# optional, it is highly recommended.
#
# The Firescars class serves as a conduit for generating and storing the
# following three binary arrays for each date (i.e., the outputs):
#
# lightMask
# anthMask
# firescars
#
# The dates are identified by parsing the filenames which reside in regimeDir.


class Firescars:
    def __init__(self, regime_dir, scar_dir, prod_dir=None):
        self.regimeDir = regime_dir
        self.scarDir = scar_dir
        self.prodDir = prod_dir

        # Identify the files in regimeDir. No error checking is done to ensure
        # that all dates in scarDir are also in regimeDir, but I have verified
        # that this is so directly. However, there are dates in regimeDir that
        # are not in scarDir.
        files = os.listdir(self.scarDir)

        # The following two lines of voodoo code create a list of the month
        # year stems present in the directory. For example: may2002, apr2007,
        # etc.
        regex = re.compile("(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)\d\d\d\d")
        self.months = list(set([v.string[0:7] for v in [m for f in files for m in [regex.search(f)] if m]]))

    def convert_all_data(self):
        # Convert data to a single raster by iterating over self.months and
        # calling writeFirescarData.
        for m in self.months:
            light_mask, anth_mask, scars_mask = self.get_firescar_data(m)
            self.write_data_to_file(light_mask, anth_mask, scars_mask, m)

    def write_data_to_file(self, light_mask, anth_mask, scars_mask, month_str):
        # Throw an exception if the intermediate products directory is not
        # defined (self.prodDir equals None).
        if self.prodDir is None:
            raise ValueError("Cannot write firescar data since prodDir is not defined")

        np.save(os.path.join(self.prodDir, month_str + "_light_mask.npy"),
                light_mask, allow_pickle=False)
        np.save(os.path.join(self.prodDir, month_str + "_anth_mask.npy"),
                anth_mask, allow_pickle=False)
        np.save(os.path.join(self.prodDir, month_str + "_scars_mask.npy"),
                scars_mask, allow_pickle=False)

    def read_data_from_file(self, month_str):
        # Throw an exception if the intermediate products directory is not
        # defined (self.prodDir equals None).
        if self.prodDir is None:
            raise ValueError("Cannot read firescar data since prodDir is not defined")

        light_mask = np.load(os.path.join(self.prodDir, month_str + "_light_mask.npy"), allow_pickle=False)
        anth_mask = np.load(os.path.join(self.prodDir, month_str + "_anth_mask.npy"), allow_pickle=False)
        scars_mask = np.load(os.path.join(self.prodDir, month_str + "_scars_mask.npy"), allow_pickle=False)
        return light_mask, anth_mask, scars_mask

    def get_firescar_data(self, month_str):
        # First check if a save file exists. If so, read it. Otherwise,
        # generate the image from the separate lightning and anthropogenic data.
        if self.prodDir is not None:
            # Checking for the existence of the following file could be moved
            # into a dedicated method since the method could also be called
            # by readDataFromFile.
            light_file = os.path.join(self.prodDir, month_str + "_light_mask.npy")
            if os.path.isfile(light_file):
                return self.read_data_from_file(month_str)

        # Read in the gdal datasets (these include the rasters)
        light_ds = gdal.Open(os.path.join(self.regimeDir, month_str + "l.tif"))
        anth_ds = gdal.Open(os.path.join(self.regimeDir, month_str + "m.tif"))
        scars_ds = gdal.Open(os.path.join(self.scarDir, month_str + ".tif"))

        # Read in the bands. There should only be one band per dataset. They
        # are converted to numpy arrays. light and anth have the following
        # values:
        #
        # -32768        No Data
        # 1                On
        # 999                Off
        #
        # scars has the following values
        #
        # 0                Off
        # 1                On
        light = np.array(light_ds.GetRasterBand(1).ReadAsArray())
        anth = np.array(anth_ds.GetRasterBand(1).ReadAsArray())
        scars = np.array(scars_ds.GetRasterBand(1).ReadAsArray())

        # Due to numpy and gdal conventions, the first index of these arrays
        # is for the width dimension (east-west) and the second for the height
        # dimension (north-south).
        if light.shape != anth.shape:
            raise ValueError("The dimensions (shape) of the lightning and human rasters do not match")
        
        if light.shape != scars.shape:
            raise ValueError("The dimensions (shape) of the lightning and firescars rasters do not match")

        # Right now, at least, the no data value is hard coded.
        no_data = -32768

        light_mask = np.empty(light.shape, dtype=np.bool)
        anth_mask = np.empty(anth.shape, dtype=np.bool)
        scars_mask = np.empty(scars.shape, dtype=np.bool)
        # Create a dictionary for mapping the lightning/human (light/anth)
        # pixel value pairs onto a raster value.
        # pixMap = dict()
        # pixMap[(no_data, no_data)] = 0
        # pixMap[(1, no_data)] = 1
        # pixMap[(999, no_data)] = 2
        # pixMap[(no_data, 1)] = 3
        # pixMap[(no_data, 999)] = 4
        # img = np.empty(light.shape, dtype=np.uint8)
        for i in range(0, light.shape[0]):
            print(i)
            for j in range(0, light.shape[1]):
                light_mask[i, j] = light[i, j] != no_data
                anth_mask[i, j] = anth[i, j] != no_data
                scars_mask[i, j] = scars[i, j] == 1
                # ky = (light[i, j], anth[i, j])
                # if not ky in pixMap:
                #     raise ValueError("Unrecognized pixel pair encountered: " + str(ky))
                # img[i, j] = pixMap[ky]
        return light_mask, anth_mask, scars_mask


# def calcScarSummaryStats(ageMosaic, ageCutoff, minSize):
#     # Calculate the following two summary statistics
#     # for an input ageMosaic: area and perimeter.
#     #
#     # ageMosaic is the array of ages in months
#     # ageCutoff is the cutoff to count a pixel as burned
#     # minSize is the minimum size of a fire to calculate
#     burned = np.less_equal(ageMosaic, ageCutoff)
#     labeled_array, num_features = measurements.label(burned)
#     slices = find_objects(labeled_array)
#
#     # Iterate over the scars to calculate statistics
#     # Could use skimage.measure.regionprops, but not
#     # all the statistics are needed so calculating
#     # only the needed ones is likely faster.
#     area = list()
#     perim = list()
#
#     for s in slices:
#         scar = burned[s]
#         numPix = np.sum(scar)
#         if numPix >= minSize:
#             perim.append(perimeter(scar))
#             area.append(numPix)
#     return(area, perim)


def calc_scar_summary_stats(burned, min_size, mask=None,
                            connectivity=generate_binary_structure(2, 2)):
    # burned and mask are both binary arrays. burned indicates whether a given
    # pixel has burned and mask indicates regions of interest in which to
    # calculate the firescar statistics. Scars are included in the calculation
    # if at least half of their pixels lie in the region of interest.
    
    # Find the fire scars
    labeled_array, num_features = measurements.label(burned, structure=connectivity)

    # Iterate over the scars to determine if they lie in the mask region. If
    # so, calculate the statistics.
    slices = find_objects(labeled_array)
    area = list()
    perim = list()
    for s in slices:
        scar = burned[s]
        if mask is not None:
            scar_masked = scar & mask[s]
        else:
            scar_masked = scar
        scar_area = np.sum(scar)
        scar_area_masked = np.sum(scar_masked)
        num_pix = np.sum(scar)
        if num_pix >= min_size and scar_area_masked >= scar_area/2:
            perim.append(perimeter(scar))
            area.append(scar_area)
    if mask is not None:
        mask_area = np.sum(mask)
    else:
        mask_area = burned.shape[0]*burned.shape[1]
    return area, perim, mask_area


def do_fire_validation(fs, config0, param_limits, num_sets, num_runs, burn_in, metric_weights):
    # Do the fire validation by looping over run sets then over runs. A run is
    # one "execution" of the model from some number of years before the first
    # fire scar validation data set date (specified by the input burnIn) through
    # the last fire scar validation data set date. A run set is a set of runs
    # all of which have the same fire validation parameters (numTrials, k_b, F,
    # and strikeRate).
    run_set_configs = create_fireval_configs(num_sets, config0, param_limits)

    metrics = np.empty((num_sets, num_runs))

    for s in range(0, num_sets):         # s for set
        for r in range(0, num_runs):     # r for run
            start_time = datetime.datetime.now()
            print("----------")
            print("Starting Set " + str(s) + " Run " + str(r))
            # Indices of metric_array:
            #   0  area metric
            #   1  perimeter metric
            #   2  proportion burned metric
            #   3  overall metric

            metric_array = do_fire_validation_run(fs, run_set_configs[s], burn_in, metric_weights)
            metrics[s, r] = np.average(metric_array[:, 3])
            stop_time = datetime.datetime.now()
            dt = stop_time - start_time
            print("Finished run in " + str(dt))

        # start_time = datetime.datetime.now()
        # print("***************")
        # print(s)
        # runMetrics = fireval.do_set_of_validation_runs(fs, trials[s], numRuns)
        # print(runMetrics)
        # metrics[s, :] = runMetrics
        # print(metrics[s, :])
        # stop_time = datetime.datetime.now()
        # dt = stop_time - start_time
        # print("Mean for " + str(numRuns) + " runs: " + str(np.mean(runMetrics)))
        # print("Finished trial in: " + str(dt))
        # print("***************")
    # print(metrics)

    return metrics


def do_fire_validation_run(fs, config, burn_in, metric_weights):
    # startTime = datetime.datetime.now()
    # Identify months at which validation data exists
    val_date = list()
    for mon in fs.months:
        # mon looks like apr2000
        mon_num = [v.lower() for v in list(calendar.month_abbr)].index(mon[0:3])
        year_num = int(mon[3:7])
        val_date.append(datetime.datetime(year_num, mon_num, 1))
    val_date.sort()
    # Start simulation fifteen years before first date (burn-in period)
    start_date = datetime.datetime(val_date[0].year - burn_in, val_date[0].month, 1)
    m = master.MartuABMSimulation(start_date, config)
    current_date = start_date
    # ageCutOff and min_size should probably be inputs to this function. They
    # could be gathered with other validation settings into a single
    # configuration variable.
    age_cutoff = 6
    min_size = 5
    metric_array = np.empty([len(val_date), 4])
    for n, d in enumerate(val_date):
        # print("\t\t************")
        # print(current_date)
        # print(d)
        num_days = d - current_date
        m.advance_by(num_days)

        # Consider getting age_mosaic in days rather than months
        # and getting exact dates of imagery on which fire scar
        # data is based.
        # Get simulated stats
        age_mosaic = get_age_mosaic(m)
        burned = np.less_equal(age_mosaic, age_cutoff)
        area_sim, perim_sim, mask_area_sim = calc_scar_summary_stats(burned, min_size)
        prop_burned_sim = np.sum(np.array(area_sim))/mask_area_sim
        # print("\t\tSimulated data")
        # print("\t\t" + str(np.sum(np.array(area_sim))/mask_area_sim))
        # print("\t\t\t Num scars:\t" + str(len(area_sim)))
        # print("\t\t\t Proportion burned:" + str(np.sum(np.array(area_sim))/mask_area_sim))

        # Get actual stats
        s = calendar.month_abbr[d.month].lower() + str(d.year)
        light_mask, anth_mask, scars_mask = fs.get_firescar_data(s)
        area, perim, mask_area = calc_scar_summary_stats(scars_mask, min_size, mask=light_mask)
        prop_burned = np.sum(np.array(area))/mask_area
        # print("Actual data")
        # print("\t Num scars:\t" + str(len(area)))
        # print("\t Proportion burned:" + str(np.sum(np.array(area))/mask_area))
        area_metric = calc_entropy(area_sim, area) * metric_weights[0]
        perim_metric = calc_entropy(perim_sim, perim) * metric_weights[1]
        prop_metric = (prop_burned_sim - prop_burned) * metric_weights[2]
        metric = math.sqrt(area_metric**2 + perim_metric**2 + prop_metric**2)
        metric_array[n, 0] = area_metric
        metric_array[n, 1] = perim_metric
        metric_array[n, 2] = prop_metric
        metric_array[n, 3] = metric
        # print("xxxx")
        # print(area_metric)
        # print(perim_metric)
        # print(prop_metric)
        # print(metric)
        # print("xxxx")

        current_date = d
    # stopTime = datetime.datetime.now()
    # dt = stopTime - startTime
    # print("-----------------------------")
    # print("Done with run in " + str(dt))
 
    return metric_array


# This repeats code in main.py -- ought to be merged
def get_age_mosaic(m):
    age_mosaic = np.zeros((m.geography.width, m.geography.height), dtype=int)
    t = m.time
    t_months = 12 * t.year + t.month
    for y in range(m.geography.height):
        for x in range(m.geography.width):
            ex_months = m.firesim.state.extinction_months[(x, y)]
            age_months = t_months - ex_months
            age_mosaic[x][y] = age_months
    return age_mosaic


def calc_entropy(y, y_ref):
    # Calculate the entropy for the sample vector y with reference sample
    # vector yRef by calling the R script calcEntropy.R. This requires the
    # reldist package to be installed in R.
    # y and yRef are lists.
    args = [str(len(y))]
    args.extend([str(v) for v in y])
    args.extend([str(v) for v in y_ref])
    cmd = ["Rscript", "calcEntropy.R"]
    cmd.extend(args)
    entropy = subprocess.check_output(cmd, universal_newlines=True)
    return float(entropy)


def create_fireval_configs(num_sets, config0, param_limits):
    # This code is not elegant, and may never be. However, it works, which is
    # a definite mark in its favor.
    #
    # This method utilizes Latin hypercube sampling to generate a set of
    # parameters with which to run do fire validation. There are four
    # parameters to be varied [example ranges in parentheses]:
    #
    # (1) The number of trials                [1 to 101]
    # (2) k_b                                [.1 to .8]
    # (3) F                                [.1 to .8]
    # (4) Lightning strike rate                [.001 to .101]
    #
    # Rounding is done to ensure that the number of trials is an integer.
    # To simplify the validation code that utilizes the trial samples, this
    # method returns a list of configurations based on the input configuration
    # config (as opposed to, say, an array).
    #
    # Since no good native Python code appears to exist for generating Latin
    # hypercube samples, they have been generated in Matlab and saved in CSV
    # files, which are read in. For example (Matlab code):
    #
    # csvwrite('lhs_20_4.csv', lhsdesign(20, 4))

    file_name = "config/lhs_" + str(num_sets) + "_4.csv"
    lhs = np.genfromtxt(file_name, delimiter=', ')

    run_set_configs = list()
    for n in range(0, num_sets):
        new_config = copy.deepcopy(config0)

        # Calculate and store number of trials
        num_trials = int(round(param_limits["num_trials"][0] + lhs[n, 0] * (param_limits["num_trials"][1] - param_limits["num_trials"][0])))
        new_config.fire.duration = "minutes=" + str(num_trials*int(config0.fire.dt.split("=")[1]))

        # Calculate and store k_b
        k_b = param_limits["k_b"][0] + lhs[n, 1] * (param_limits["k_b"][1] - param_limits["k_b"][0])
        new_config.fire.haydon.k_b = k_b

        # Calculate and store F
        f = param_limits["F"][0] + lhs[n, 2] * (param_limits["F"][1] - param_limits["F"][0])
        new_config.fire.haydon.F = f

        # Calculate and store strike rate
        strike_rate = param_limits["strike_rate"][0] + lhs[n, 3] * (param_limits["strike_rate"][1] - param_limits["strike_rate"][0])
        new_config.fire.strike_rate_km_day = strike_rate
        run_set_configs.append(new_config)
        return run_set_configs
