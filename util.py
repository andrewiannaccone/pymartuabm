import bisect
import collections
import datetime
import numpy


def timedelta_from_str(s):
    p = eval('dict(%s)' % s)
    return datetime.timedelta(**p)


def rgb_uint_to_arr(x: numpy.uint32, arr):
    arr[0] = (int(x) >> 16) % 256
    arr[1] = (int(x) >> 8) % 256
    arr[2] = int(x) % 256
    return arr


def rgb_arr_to_uint(arr):
    [r, g, b] = arr
    x = 0x00000000
    x += int(r)
    x <<= 8
    x += int(g)
    x <<= 8
    x += int(b)
    return x

temp_xarr = [0, 0, 0]
temp_yarr = [0, 0, 0]


def rgb_weighted(x, y, w):
    assert 0 <= w <= 1

    rx = (x >> 16) % 256
    ry = (y >> 16) % 256
    r = int(w * rx + (1-w) * ry)

    gx = (x >> 8) % 256
    gy = (y >> 8) % 256
    g = int(w * gx + (1-w) * gy)

    bx = x % 256
    by = y % 256
    b = int(w * bx + (1-w) * by)

    return (r << 16) + (g << 8) + b


# def rgb_weighted(x: numpy.uint32, y: numpy.uint32, w: float):
#     xa = rgb_uint_to_arr(x, temp_xarr)
#     ya = rgb_uint_to_arr(y, temp_yarr)
#     outa = numpy.add(numpy.multiply(w, xa), numpy.multiply(1-w, ya))
#     return rgb_arr_to_uint(outa)


class EventQueue:
    def __init__(self, length):
        self.times = collections.deque([], length)
        self.events = collections.deque([], length)

    def add_event(self, t, e):
        if self.times and self.times[-1] > t:
            raise ValueError(t)

        self.times.append(t)
        self.events.append(e)

    def events_since(self, t):
        idx = bisect.bisect(self.times, t)
        for i in range(idx, len(self.times)):
            yield self.events[i]
