import os
# python do_fire_sim_validation.py "C:\Users\mhp12\Box Sync\Bird_Lab\Research\projects\MartuABM\data"
import sys
import yaml
import bunch3
from validation import fireval


def main():

    # "Extract" the data directory from the run line input
    data_dir = sys.argv[1]
    # Load the fire scar validation data
    fs_dir = os.path.join(data_dir, "2000-2010RasterScarMaps", "geotiffs")
    regime_dir = os.path.join(fs_dir, "regime")
    scars_dir = os.path.join(fs_dir, "scars")
    prod_dir = os.path.join(fs_dir, "prod")
    fs = fireval.Firescars(regime_dir, scars_dir, prod_dir)

    # Dictionary of parameter limits
    param_limits = {
        'numTrials': [1, 101],
        'k_b': [.1, .8],
        'F': [1, 8],
        'strikeRate': [.001, .101]
    }

    # Number of run sets
    num_sets = 20

    # Number of runs per set
    num_runs = 2

    # Read in the baseline configuration
    with open('config/models.yml') as fd:
        config0 = bunch3.bunchify(yaml.load(fd))
  
    metric_weights = [1, 1, 1]

    burn_in = 15

    # Do the validation
    metrics = fireval.do_fire_validation(fs, config0, param_limits, num_sets,
                                         num_runs, burn_in, metric_weights)
    print(metrics)

if __name__ == '__main__':
    main()
