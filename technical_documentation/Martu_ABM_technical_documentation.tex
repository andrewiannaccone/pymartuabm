\documentclass[11pt]{article}
\usepackage[round,authoryear]{natbib}
\bibpunct[:]{(}{)}{;}{a}{}{,}
\usepackage[pdftex, colorlinks=true, pdfstartview=FitH, linkcolor=blue, 
      citecolor=blue, urlcolor=blue]{hyperref}
\usepackage{amsmath}

\begin{document}
\title{An agent based model of human foraging and fire ecology in the Western Desert of Australia}

\author{Michael Holton Price\\
	michaelholtonprice@gmail.com}
\normalsize
\date{}

\maketitle
\section{Overview}
%1982 Winter
%
%Mala
%Raptors* > Perentes? > Dingos* > Cat* > Fox?
%
%
%For each agent
%-----------------
%What they eat
%What eats them
%Home range (foraging pixel size)
%Hunting pixel range
%Vision range
This document provides the technical description of an agent based model (ABM) of human foraging and fire ecology in the Western Desert of Australia. It contains five sections: (1) this overview; (2) a discussion of model conception and requirements; (3) an algorithm description; (4) a software implementation description; and (5) a validation plan. The algorithm description is explicitly agnostic regarding the software implementation, which can be in any appropriate programming language (e.g., C++, Java, Python, NetLogo, etc.). We have chosen to implement the ABM using Python, as described in the software implementation section.

In systems engineering, a distinction is typically made between validation and verification. Validation asks the question: Is the algorithm appropriate for its intended purpose? For example, are the assumptions of the algorithm realistic? When an algorithm, or some aspect of it, models a physical system, validation can proceed directly by assessing whether the model's outputs match reality. When direct empirical validation is infeasible, however, alternative approaches are needed, such as review by domain experts.

Verification, in contrast, asks the question: Was the algorithm implemented as intended? A common means of verification is to input simple data for which outputs can be analytically (or otherwise simply) predicted, or to input data from an existing implementation of the algorithm. The algorithm/model, or that sub-component being verified, is accepted if the outputs match the predictions from the alternative source. Verification most closely corresponds to the concept of test used in software development. We have chosen not to implement an explicit verification plan, trusting the validation plan to appropriately assess the models accuracy.

\section{Fire and plant growth}
\subsection{Spinifex}
The model for spinifex growth is based on \citet{southgate_carthew200}, utilized a generalized linear model to fit ground cover in the Tanami Desert (total ground cover, spinifex cover, and \emph{Yakirra australiense}). For spinifex, the key variables that influence cover are mean annual rainfall, the square of mean annual rainfall, and years since fire (see \citet{southgate_carthew2007}, Table 3). The ground cover of spinifex is given by the logit equation,
\begin{equation}
  \label{eq:logit}
    y = \frac{e^{u}}{1 + e^u} \mbox{,}
\end{equation}
where $u$ is the linear predictor of cover, which \citet{southgate_carthew2007} estimate to be
\label{eq:logit}
  \begin{multline}
    u = -7.119 + 30.932*10^{-3} \, \frac{1}{\mathrm{mm}} \, (\textrm{Mean Annual Rainfall})\\
        - 0.413*10^{-4} \, \frac{1}{\mathrm{mm}^2} \, (\textrm{Mean Annual Rainfall})^2\\
        + 0.055 \, \frac{1}{\mathrm{yr}} \, (\textrm{Age Since Fire}) \mbox{.}
\end{multline}


\section{Animal Sims}
\subsection{Rodents and Rodent-like Marsupials}
We have chosen to model rodents and rodent-like marsupials as a single animal class with one sim. The primary sources that we relied on in designing the sim are the work of Letnic, Dickman, and colleagues and those whom they cite \citep{letnic_dickman2010,dickman_etal2010}. In contrast to lizards and large mammals, the population numbers of small ``rodents'' (including rodent-like marsupials) exhibit relatively little dependence on fire age or mosaic structure. Instead, population numbers mostly track rain pulses with a slight delay. One priviso is that the predation risk of rodents increases after fires when cover is lost. However, is not necessarily the main predation risk other than, possibly, immediately after a fire. However, the boost in hunting return rates is very important for the population dynamics of meso-predators that eat rodents, such as cats.

In arid Australia, rodents can travel substantial distances (> 5 km) to find areas with food. Since our present rain model has no spatial variability (i.e., daily rain is the same across the entire area) the mobility of rodents suggests that the population can be modeled as uniform across the entire landscape. This assumption is not reliable if rain is localized. Consequently, we model landscape wide population numbers using a modified form of the Lotka-Volterra equation. The differential equation governing rodent population size is
\begin{equation}
  \label{eq:rodent_differential_equation}
  \frac{dN_r}{dt} = [\alpha_r - \beta_r] N_r \mbox{,}
\end{equation}
where $\alpha_r$ is the growth rate and $\beta_r$ is the predation rate. $\alpha_r$ will now be defined and $\beta_r$ is defined in the section on predation. Growth is modeled as a modified form of the logistic equation in which the carrying capacity varies with time as a function of cumulative rainfall in the past 90 days,
\begin{equation}
  \label{eq:rodent_carrying_capacity}
  K_r = \frac{K_{r,min} - K_{r,max}\,S_r(0) + [K_{r,max} - K_{r,min}]S_r({rain}_{90})}{1-S_r(0)}  \mbox{,}
\end{equation}
where $K_{r,min}$ is the minimum rodent carrying capacity during drought, $K_{r,max}$ is the maximum rodent carrying capacity, ${rain}_{90}$ is the cumulative rain in millimeters over the past 90 days, and
\begin{equation}
  \label{eq:rodent_carrying_capacity}
  S_r({rain}_{90}) = \frac{1}{1 + \exp(-a_r \, {rain}_{90})}
\end{equation}
is the sigmoid function with shape parameter $a_r$ for rodents. The growth term in Equation~\ref{eq:rodent_differential_equation} is
\begin{equation}
  \label{eq:rodent_carrying_capacity}
  \alpha_r = k_r \, \frac{K_r - N_r}{K_r}\mbox{,}
\end{equation}
where $k_r$ is the Malthusian parameter for rodents, or maximum growth rate.

\subsection{Predation}
In contrast to rodents, we model cats and other predators as individual agents on the landscape with specific locations and adaptive strategies depending on their local terrain. Cat hunting is modeled using optimal foraging theory (OFT). In particular, a cat occupying a given habitat or super-pixel (an aggregation of landscape pixels into a larger unit -- e.g., a 10 by 10 group of pixels is a single habitat) chooses a diet breadth per the diet choice model, which gives an immediate return rate in that habitat. The cat can move to an adjacent habitat by expending some amount of energy. The strategy for when to move is governed my a genetic algorithm that takes into account the return rate of the current habitat and return rates of adjacent habitats, which are all assumed to be known to the cat.


\bibliographystyle{plainnat}
\bibliography{martu_abm_bib}
\end{document}

